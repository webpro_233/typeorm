import { Column, Entity, PrimaryGeneratedColumn, } from "typeorm";
@Entity()
export class Product{
    @PrimaryGeneratedColumn({name:"prodect_id"})
    id:number;
    @Column()
    name:string;
    @Column()
    price: number;


}